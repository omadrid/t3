# T3

Trust Tree Traversal

## Decentralized ID (DID)

T3 uses DID to build trust scores across your social world.

## Trust

There are two trust actions, Trust and Distrust. These actions are written to the blockchain. Trust is given to entities. Entities can consist of either a 'owner' or 'artifact'.

## Registering

- uPort (https://developer.uport.me/guides/gettingstarted)
- Show immidiate tree of close connections in GUI
- Simple Trust & Distrust buttons

## Traversing the Trust Tree

1) Pick an entry point (default is self)
2) Return all entites from the entry point
3) nth degree should have been provided by application.

## Applications

- Minds Channels (eg. minds.com/mark)
- Minds Posts
- Twitter Posts via API
