const express = require("express");
const bodyParser = require("body-parser");
const ngrok = require("ngrok");
const decodeJWT = require("did-jwt").decodeJWT;
const { Credentials } = require("uport-credentials");
const transports = require("uport-transports").transport;
const message = require("uport-transports").message.util;

let endpoint = "";
const app = express();
app.use(bodyParser.json({ type: "*/*" }));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type");

  next();
});

//setup Credentials object with newly created application identity.
const credentials = new Credentials({
  appName: "Login Example",
  did: "did:ethr:0x31486054a6ad2c0b685cd89ce0ba018e210d504e",
  privateKey: "ef6a01d0d98ba08bd23ee8b0c650076c65d629560940de9935d0f46f00679e01"
});

/**
 * Collect a QR code and return to the client
 */
app.get("/qr", async (req, res) => {
  try {
    const requestToken = await credentials.createDisclosureRequest({
      requested: ["name"],
      notifications: true,
      callbackUrl: endpoint + "/callback"
    });
    console.log(decodeJWT(requestToken)); //log request token to console
    const uri = message.paramsToQueryString(
      message.messageToURI(requestToken),
      { callback_type: "post" }
    );
    const qr = transports.ui.getImageDataURI(uri);
    res.send({
      qrImageData: qr
    });
  } catch (err) {}
});

app.post("/callback", async (req, res) => {
  const jwt = req.body.access_token;
  console.log(jwt);

  try {
    const credentials = await credentials.authenticateDisclosureResponse(jwt);
    console.log("[credentials]: ", credentials);
  } catch (err) {
    console.log(err);
  }
});
