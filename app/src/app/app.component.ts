import { Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
// import * as Connect from "uport-connect";

declare const Connect: any;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "t3";
  qrImageUri: SafeUrl;

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) {}

  ngOnInit() {
    // const uport = new Connect("Minds Trust (t3)", );
    // uport.requestDisclosure();
    this.fetchQrImageUri();
  }

  async fetchQrImageUri() {
    this.http.get("http://localhost:8088/qr").subscribe((data: any) => {
      this.qrImageUri = this.sanitizer.bypassSecurityTrustUrl(data.qrImageData);
    });
  }
}
